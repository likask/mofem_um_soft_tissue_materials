set command mbconvert;
if command --search $command >/dev/null
  for file in out_*.h5m
    set no_base (basename $file .h5m)
    set vtk_base "$no_base.vtk"
    mbconvert $file $vtk_base
  end
else
  echo "mbconvert not found. Add to PATH"
end
