#! /usr/bin/fish
echo "Save current users_modules directory"
set dir_usr_mod (pwd)
echo "Build libs"
echo ""
cd ../../lib
make -j 4;
echo ""
echo "Install User's Modules"
echo ""
make install;

echo ""
echo "cmake users_modules"
echo ""
cd ../usr
cmake -DCMAKE_CXX_FLAGS="-I/Users/euan/build/mofem/usr/include -Wno-bind-to-temporary-copy -Wno-overloaded-virtual" -DCMAKE_EXE_LINKER_FLAGS=-L/Users/euan/build/mofem/usr/lib -DCMAKE_BUILD_TYPE=Release users_modules/
echo ""
echo "Build: $dir_usr_mod"
echo ""
cd $dir_usr_mod
make -j 4;
