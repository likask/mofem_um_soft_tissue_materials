/** \file EberleinFibres_NeoHookean.hpp
 * \ingroup nonlinear_elastic_elem
 * \brief Implementation of Eberlein fibres elastic materials
 */

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __EBERLEINFIBRES_HPP__
#define __EBERLEINFIBRES_HPP__

/** \brief Eberlein Fibres equation
  * \ingroup nonlinear_elastic_elem
  * Inherit from NeoHookean
  */

template<typename TYPE>
struct EberleinFibres_NeoHookean: public  NeoHookean<TYPE> {

  EberleinFibres_NeoHookean(): NeoHookean<TYPE>() {}

  /** \brief
   * <B>Eberlein Fibres</B>
   *
   * Helmholtz energy:
   *
   * \f$\bar{\Psi}_f = \sum_{\alpha=1}^{2} \frac{k_1}{2k_2} \{[ \textrm{exp}  [k_2(\bar{I}_{\alpha}^* - 1)^2]-1] \}\f$
   *
   * Second Piola-Kirchoff Stress:
   *
   * \f$\bar{\mathbf{S}}_f = J^{-2/3}\mathbf{P} : \hat{\mathbf{S}}_f\f$
   *
   * where \f$\hat{\mathbf{S}}_f\f$ is a fictitious stress:
   *
   * \f$\hat{\mathbf{S}}_f = \sum_{\alpha=1}^{2} 2k_1 \{ \textrm{exp}  [k_2(\bar{I}_{\alpha}^* - 1)^2](\bar{I}_{\alpha}^* - 1)\mathbf{A}_\alpha \}\f$
   *
   * and \f$\mathbf{P}\f$ is a 4th order projection tensor: \f$\mathbf{P} = \mathbf{I} - \frac{1}{3}\mathbf{C}^{-1} \otimes \mathbf{C}\f$
   *
   * \f$\mathbf{A}_\alpha = \mathbf{a}_\alpha \otimes \mathbf{a}_\alpha\f$
   *
   * \f$\mathbf{\bar{C}} = J^{-2/3} \mathbf{C}\f$
   *
   * \f$\bar{I}_{\alpha}^* = \lambda_\alpha^2\f$
   *
   * \f$\bar{I}_{\alpha}^* = \mathbf{\bar{C}} : \mathbf{A}_\alpha, \quad \alpha = 1,2\f$
   *
   For details look to: <br>
   \cite Eberlein_Holzapfel_2001
*/
  ublas::matrix<TYPE> C_bar, A1, A2, Q;
  ublas::matrix<TYPE> AB_T;
  ublas::vector<TYPE> a1, a2;
  TYPE I_bar1, I_bar2;
  TYPE k1,k2;
  TYPE DenJPowTwoThird;
  TYPE I;
  ublas::matrix<TYPE> Sf1, Sf2, Sf;

  ublas::vector<double> a1_double;
  ublas::vector<double> a2_double;

  PetscErrorCode ierr;

  PetscErrorCode CalculateDenJPowTwoThird(){
      PetscFunctionBegin;
      DenJPowTwoThird = 1.0/pow(this->J,(2.0/3.0));
      PetscFunctionReturn(0);
  }

  PetscErrorCode DoubleInnerProd(ublas::matrix<TYPE> &A,ublas::matrix<TYPE> &B, int n, TYPE &A_inP_B){
    PetscFunctionBegin;
    for(int i = 0;i<n;i++) {
      for(int j = 0;j<n;j++) {
        A_inP_B += A(i,j)*B(i,j);
      }
    }
    PetscFunctionReturn(0);
  }

  PetscErrorCode CalculateCbar(){
    PetscFunctionBegin;
    //Note: J definition may be different and involve sqrt
    C_bar.resize(3,3);
    noalias(C_bar) = this->C;
    C_bar *= DenJPowTwoThird;
    PetscFunctionReturn(0);
  }
  PetscErrorCode CalculateA(){
    PetscFunctionBegin;
    A1.resize(3,3);
    A2.resize(3,3);
    A1.clear();
    A2.clear();
    //a1 changing in value
    cout << "a1: " << a1 << endl;
    cout << "a2: " << a2 << endl;
    A1 = outer_prod(a1,a1);
    A2 = outer_prod(a2,a2);
    //A1 and A2 should be constant but not
    // cout << "A1: " << A1 << endl;
    // cout << "A2: " << A2 << endl;
    PetscFunctionReturn(0);
  }

  PetscErrorCode CalculateIbar(){
    PetscFunctionBegin;
    I_bar1=0;
    I_bar2=0;
    DoubleInnerProd(C_bar,A1,3,I_bar1);
    DoubleInnerProd(C_bar,A2,3,I_bar2);
    PetscFunctionReturn(0);
  }

  PetscErrorCode EberleinFibres_PiolaKirchhoffII() {
    PetscFunctionBegin;
    Sf1.resize(3,3);
    Sf1.clear();
    Sf2.resize(3,3);
    Sf2.clear();
    //this->S.clear(); //don't do this otherwise you'll lose the energy from the base class
    // < 1 and NaN
    cout << "I_bar1: " << I_bar1 << endl;
    cout << "I_bar2: " << I_bar2 << endl;
    if (I_bar1 > 1.0) {
    noalias(Sf1) = A1;
    Sf1 *= 2*k1*(exp(k2*pow((I_bar1-1),2)))*(I_bar1-1);
    }
    if (I_bar2 > 1.0) {
    noalias(Sf2) = A2;
    Sf2 *= 2*k1*(exp(k2*pow((I_bar2-1),2)))*(I_bar2-1);
    }
    Sf.resize(3,3);
    Sf.clear();
    Sf = Sf1 + Sf2;
    // NaN
    // cout << "Sf: " << Sf << endl;
    Q.resize(3,3);
    I = 0;
    //cout << "S1 " << this->S << endl;
    for(int i = 0;i<3;i++) {
        for(int j = 0;j<3;j++) {
            Q(i,j) = 0;
            for(int k = 0;k<3;k++) {
                for(int l = 0;l<3;l++) {
                    if (i==j && j==k && k==l) {
                        I = 1.0;
                    }
                    else { I = 0;}
                    Q(i,j) += DenJPowTwoThird*(I - (1.0/3.0)*this->invC(i,j)*this->C(k,l))*Sf(i,j);
                }
            }
        }
    }
    this->S += Q;
    PetscFunctionReturn(0);
  }

  virtual PetscErrorCode calculateDoubleFibresDirection() {
    PetscFunctionBegin;
    PetscFunctionReturn(0);
  }

    virtual PetscErrorCode calculateP_PiolaKirchhoffI(
      const NonlinearElasticElement::BlockData block_data,
      const NumeredMoFEMFiniteElement *fe_ptr) {
      PetscFunctionBegin;
      this->S.resize(3,3);
      this->S.clear();
      //NeoHookean
      this->lambda = LAMBDA(block_data.E,block_data.PoissonRatio);
      this->mu = MU(block_data.E,block_data.PoissonRatio);
      ierr = this->calculateC_CauchyDefromationTensor(); CHKERRQ(ierr);
      ierr = this->NeoHooke_PiolaKirchhoffII(); CHKERRQ(ierr);
      //+Fibres
      k1 = block_data.k1;
      k2 = block_data.k2;
      a1.resize(3,false); a2.resize(3,false);
      // a1.clear(); a2.clear();
      // a1[0] = block_data.d1_X; a1[1] = block_data.d1_Y; a1[2] = block_data.d1_Z;
      // a2[0] = block_data.d2_X; a2[1] = block_data.d2_Y; a2[2] = block_data.d2_Z;
      ierr = calculateDoubleFibresDirection(); CHKERRQ(ierr);
      ierr = CalculateDenJPowTwoThird(); CHKERRQ(ierr);
      ierr = CalculateCbar(); CHKERRQ(ierr);
      ierr = CalculateA(); CHKERRQ(ierr);
      ierr = CalculateIbar(); CHKERRQ(ierr);
      ierr = EberleinFibres_PiolaKirchhoffII(); CHKERRQ(ierr);
      this->P.resize(3,3);
      noalias(this->P) = prod(this->F,this->S);
      PetscFunctionReturn(0);
    }

    //UNTESTED
    //needs updating - copy stress
    PetscErrorCode EberleinFibres_ElasticEnergy(){
      PetscFunctionBegin;
      if (I_bar1 > 1.0) {
        this->eNergy += (k1/(2*k2))*(exp(k2*pow((I_bar1-1),2))-1);
      }
      if (I_bar2 > 1.0) {
        this->eNergy += (k1/(2*k2))*(exp(k2*pow((I_bar2-1),2))-1);
      }
      PetscFunctionReturn(0);
    }
    //UNTESTED
    virtual PetscErrorCode calculateElasticEnergy(
      const NonlinearElasticElement::BlockData block_data,
      const NumeredMoFEMFiniteElement *fe_ptr) {
      PetscFunctionBegin;
      PetscErrorCode ierr;
      this->eNergy = 0;
      //NeoHookean
      this->lambda = LAMBDA(block_data.E,block_data.PoissonRatio);
      this->mu = MU(block_data.E,block_data.PoissonRatio);
      ierr = this->calculateC_CauchyDefromationTensor(); CHKERRQ(ierr);
      ierr = this->dEterminatnt(this->F,this->J); CHKERRQ(ierr);
      ierr = this->NeoHookean_ElasticEnergy(); CHKERRQ(ierr);
      //+Fibres
      k1 = block_data.k1;
      k2 = block_data.k2;
      a1.resize(3); a2.resize(3);
      // a1.clear(); a2.clear();
      // a1[0] = block_data.d1_X; a1[1] = block_data.d1_Y; a1[2] = block_data.d1_Z;
      // a2[0] = block_data.d2_X; a2[1] = block_data.d2_Y; a2[2] = block_data.d2_Z;
      ierr = CalculateDenJPowTwoThird(); CHKERRQ(ierr);
      ierr = CalculateCbar(); CHKERRQ(ierr);
      ierr = CalculateA(); CHKERRQ(ierr);
      ierr = CalculateIbar(); CHKERRQ(ierr);
      ierr = EberleinFibres_ElasticEnergy(); CHKERRQ(ierr);
      PetscFunctionReturn(0);
    }

  //this is only used in derived class
  PetscErrorCode calculateFieldFibresDirection() {
    PetscFunctionBegin;
    // int gg = this->gG; // number of integration point

    // ublas::vector<double> &a1_ref = (this->commonDataPtr->dataAtGaussPts["FIBRES_DIRECTION_1"][gg]);
    // ublas::vector<double> &a2_ref = (this->commonDataPtr->dataAtGaussPts["FIBRES_DIRECTION_2"][gg]);

    a1_double.resize(3,false);
    a2_double.resize(3,false);
    // a1_double(0) = a1_ref(0); a1_double(1) = a1_ref(1); a1_double(2) = a1_ref(2);
    // a2_double(0) = a2_ref(0); a2_double(1) = a2_ref(1); a2_double(2) = a2_ref(2);

    a1_double[0] = 1; a1_double[1] = 0; a1_double[2] = 0;
    a2_double[0] = 0; a2_double[1] = 1; a2_double[2] = 0;

    PetscFunctionReturn(0);
  }

};


//Post Processing
struct EberleinFibres_NeoHookeanDouble: public EberleinFibres_NeoHookean<double> {

  EberleinFibres_NeoHookeanDouble(): EberleinFibres_NeoHookean<double>() {}

  virtual PetscErrorCode calculateDoubleFibresDirection () {
    PetscFunctionBegin;
    try {

      ierr = calculateFieldFibresDirection(); CHKERRQ(ierr);

    } catch (const std::exception& ex) {
      ostringstream ss;
      ss << "throw in method: " << ex.what() << endl;
      SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
    }
    PetscFunctionReturn(0);
  }
  // virtual PetscErrorCode getDataOnPostProcessor(
  //   map<string,vector<ublas::vector<double> > > &field_map,
  //   map<string,vector<ublas::matrix<double> > > &grad_map
  // ) {
  //   PetscFunctionBegin;
  //   int nb_gauss_pts = grad_map["FIBRES_DIRECTION_1"].size();
  //   this->commonDataPtr->dataAtGaussPts["FIBRES_DIRECTION_1"].resize(nb_gauss_pts);
  //   for(int gg = 0;gg<nb_gauss_pts;gg++) {
  //     this->commonDataPtr->dataAtGaussPts["FIBRES_DIRECTION_1"][gg].resize(1,3,false);
  //     for(int ii = 0;ii<3;ii++) {
  //       this->commonDataPtr->gradAtGaussPts["FIBRES_DIRECTION_1"][gg](0,ii) =
  //       ((grad_map["POTENTIAL_FIELD"])[gg])(0,ii);
  //     }
  //   }
  //   PetscFunctionReturn(0);
  // }
};

struct EberleinFibres_NeoHookeanADouble: public EberleinFibres_NeoHookean<adouble> {

  EberleinFibres_NeoHookeanADouble(): EberleinFibres_NeoHookean<adouble>() {}

  int nbActiveVariables0;

  //Specify which variables are active
  virtual PetscErrorCode setUserActiveVariables(
    int &nb_active_variables
  ) {
    PetscFunctionBegin;

    try {

      ierr = calculateFieldFibresDirection(); CHKERRQ(ierr);

      a1.resize(3,false);
      a2.resize(3,false);
      a1.clear(); a2.clear();
      a1[0] <<= a1_double(0); a1[1] <<= a1_double(1); a1[2] <<= a1_double(2);
      a2[0] <<= a2_double(0); a2[1] <<= a2_double(1); a2[2] <<= a2_double(2);
      nbActiveVariables0 = nb_active_variables;
      nb_active_variables += 6;

    } catch (const std::exception& ex) {
      ostringstream ss;
      ss << "throw in method: " << ex.what() << endl;
      SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
    }

    PetscFunctionReturn(0);
  }

  //Assign values to active variables
  virtual PetscErrorCode setUserActiveVariables(
    ublas::vector<double> &active_varibles) {
    PetscFunctionBegin;

    try {

      ierr = calculateFieldFibresDirection(); CHKERRQ(ierr);

      int shift = nbActiveVariables0; // is the number of elements in F
      active_varibles[shift+0] = a1_double(0);
      active_varibles[shift+1] = a1_double(1);
      active_varibles[shift+2] = a1_double(2);
      active_varibles[shift+3] = a2_double(0);
      active_varibles[shift+4] = a2_double(1);
      active_varibles[shift+5] = a2_double(2);

    } catch (const std::exception& ex) {
      ostringstream ss;
      ss << "throw in method: " << ex.what() << endl;
      SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
    }

    PetscFunctionReturn(0);
  }

};

#endif
