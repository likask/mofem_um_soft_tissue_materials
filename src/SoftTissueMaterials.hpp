/** \file SoftTissueMaterials.hpp
 * \ingroup nonlinear_elastic_elem
 * \brief Elastic materials
 */

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __SOFTTISSUEMATERIALS_HPP__
#define __SOFTTISSUEMATERIALS_HPP__

#include <NeoHookean.hpp>
#include <EberleinFibres_NeoHookean.hpp>

#define MAT_EBERLEINFIBRES_NEOHOOKEAN "EBERLEINFIBRES_NEOHOOKEAN"

/** \brief Manage setting parameters and constitutive equations for nonlinear/linear elastic materials
  * \ingroup nonlinear_elastic_elem
  */
struct SoftTissueMaterials{

  FieldInterface &mField;
  string defMaterial;
  string configFile;

  bool iNitialized;

  SoftTissueMaterials(FieldInterface &m_field):
    mField(m_field),defMaterial(MAT_EBERLEINFIBRES_NEOHOOKEAN),
    configFile("elastic_material.in"),
    iNitialized(false) {}


  boost::ptr_map<string,NonlinearElasticElement::FunctionsToCalculatePiolaKirchhoffI<adouble> > aDoubleMaterialModel;
  boost::ptr_map<string,NonlinearElasticElement::FunctionsToCalculatePiolaKirchhoffI<double> > doubleMaterialModel;

  struct BlockOptionData {
    string mAterial;
    int oRder;
    double yOung;
    double pOisson;
    double f_k1,f_k2;
    double f_d1_X,f_d1_Y,f_d1_Z;
    double f_d2_X,f_d2_Y,f_d2_Z;
    BlockOptionData():
    mAterial(MAT_EBERLEINFIBRES_NEOHOOKEAN),
    oRder(-1),
    yOung(-1),
    pOisson(-2),
    f_k1(-1),
    f_k2(-1),
    f_d1_X(0),f_d1_Y(0),f_d1_Z(0),
    f_d2_X(0),f_d2_Y(0),f_d2_Z(0)
    {}
  };
  map<int,BlockOptionData> blockData;

  PetscBool isConfigFileSet;
  po::variables_map vM;


  virtual PetscErrorCode iNit() {
    PetscFunctionBegin;
    //add new material below
    string mat_name;
    mat_name = MAT_EBERLEINFIBRES_NEOHOOKEAN;
    // aDoubleMaterialModel.insert(mat_name,new EberleinFibres_NeoHookean<adouble>());
    doubleMaterialModel.insert(mat_name,new EberleinFibres_NeoHookeanDouble());
    aDoubleMaterialModel.insert(mat_name,new EberleinFibres_NeoHookeanADouble());
    // doubleMaterialModel.insert(mat_name,new EberleinFibres_NeoHookeanDouble());
    ostringstream avilable_materials;
    avilable_materials << "set elastic material < ";
    boost::ptr_map<string,NonlinearElasticElement::FunctionsToCalculatePiolaKirchhoffI<double> >::iterator mit;
    mit = doubleMaterialModel.begin();
    for(;mit!=doubleMaterialModel.end();mit++) {
      avilable_materials << mit->first << " ";
    }
    avilable_materials << ">";
    PetscErrorCode ierr;
    ierr = PetscOptionsBegin(mField.get_comm(),"","Elastic Materials Configuration","none"); CHKERRQ(ierr);
    char default_material[255];
    PetscBool def_mat_set;
    ierr = PetscOptionsString("-default_material",avilable_materials.str().c_str(),"",MAT_EBERLEINFIBRES_NEOHOOKEAN,default_material,255,&def_mat_set); CHKERRQ(ierr);
    if(def_mat_set) {
      defMaterial = default_material;
      if(aDoubleMaterialModel.find(defMaterial)==aDoubleMaterialModel.end()) {
        SETERRQ1(PETSC_COMM_SELF,MOFEM_NOT_IMPLEMENTED,"material <%s> not implemented",default_material);
      }
    }
    char config_file[255];
    ierr = PetscOptionsString("-elastic_material_configuration","elastic materials configure file name","",configFile.c_str(),config_file,255,&isConfigFileSet); CHKERRQ(ierr);
    if(isConfigFileSet) {
      configFile = config_file;

    }
    ierr = PetscOptionsEnd(); CHKERRQ(ierr);
    PetscFunctionReturn(0);
  }

  /** \brief read Elastic materials declaration for blocks and meshsets

    User has to include in file header:
    \code
    #include <boost/program_options.hpp>
    using namespace std;
    namespace po = boost::program_options;
    \endcode

    File parameters:
    \code
    [block_1]
    displacemet_order = 1/2 .. N
    material = EBERLEINFIBRES_NEOHOOKEAN
    young_modulus = 1
    poisson_ratio = 0
    f_k1 = 10
    f_k2 = 50
    f_d1_X = 1
    f_d1_Y = 0
    f_d1_Z = 0
    f_d2_X = 0
    f_d2_Y = 0
    f_d2_Z = 1
    \endcode

    To read material configuration file you need to use option:
    \code
    -elastic_material_configuration name_of_config_file
    \endcode

    */
  virtual PetscErrorCode readConfigFile() {
    PetscFunctionBegin;
    PetscErrorCode ierr;
    try {

      po::options_description config_file_options;
      for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField,BLOCKSET,it)) {

        ostringstream str_order;
        str_order << "block_" << it->get_msId() << ".displacemet_order";
        config_file_options.add_options()
        (str_order.str().c_str(),po::value<int>(&blockData[it->get_msId()].oRder)->default_value(-1));

        ostringstream str_material;
        str_material << "block_" << it->get_msId() << ".material";
        config_file_options.add_options()
        (str_material.str().c_str(),po::value<string>(&blockData[it->get_msId()].mAterial)->default_value(defMaterial));

        ostringstream str_ym;
        str_ym << "block_" << it->get_msId() << ".young_modulus";
        config_file_options.add_options()
        (str_ym.str().c_str(),po::value<double>(&blockData[it->get_msId()].yOung)->default_value(-1));

        ostringstream str_pr;
        str_pr << "block_" << it->get_msId() << ".poisson_ratio";
        config_file_options.add_options()
        (str_pr.str().c_str(),po::value<double>(&blockData[it->get_msId()].pOisson)->default_value(-2));

        ostringstream str_f_k1;
        str_f_k1 << "block_" << it->get_msId() << ".f_k1";
        config_file_options.add_options()
          (str_f_k1.str().c_str(),po::value<double>(&blockData[it->get_msId()].f_k1)->default_value(-1));

        ostringstream str_f_k2;
        str_f_k2 << "block_" << it->get_msId() << ".f_k2";
        config_file_options.add_options()
          (str_f_k2.str().c_str(),po::value<double>(&blockData[it->get_msId()].f_k2)->default_value(-1));

        ostringstream str_f_d1_X;
        str_f_d1_X << "block_" << it->get_msId() << ".f_d1_X";
        config_file_options.add_options()
          (str_f_d1_X.str().c_str(),po::value<double>(&blockData[it->get_msId()].f_d1_X)->default_value(0));

        ostringstream str_f_d1_Y;
        str_f_d1_Y << "block_" << it->get_msId() << ".f_d1_Y";
        config_file_options.add_options()
          (str_f_d1_Y.str().c_str(),po::value<double>(&blockData[it->get_msId()].f_d1_Y)->default_value(0));

        ostringstream str_f_d1_Z;
        str_f_d1_Z << "block_" << it->get_msId() << ".f_d1_Z";
        config_file_options.add_options()
          (str_f_d1_Z.str().c_str(),po::value<double>(&blockData[it->get_msId()].f_d1_Z)->default_value(0));

        ostringstream str_f_d2_X;
        str_f_d2_X << "block_" << it->get_msId() << ".f_d2_X";
        config_file_options.add_options()
          (str_f_d2_X.str().c_str(),po::value<double>(&blockData[it->get_msId()].f_d2_X)->default_value(0));

        ostringstream str_f_d2_Y;
        str_f_d2_Y << "block_" << it->get_msId() << ".f_d2_Y";
        config_file_options.add_options()
          (str_f_d2_Y.str().c_str(),po::value<double>(&blockData[it->get_msId()].f_d2_Y)->default_value(0));

        ostringstream str_f_d2_Z;
        str_f_d2_Z << "block_" << it->get_msId() << ".f_d2_Z";
        config_file_options.add_options()
          (str_f_d2_Z.str().c_str(),po::value<double>(&blockData[it->get_msId()].f_d2_Z)->default_value(0));
      }

      ifstream file(configFile.c_str());
      if(isConfigFileSet) {
        if(!file.good()) {
          SETERRQ1(PETSC_COMM_SELF,MOFEM_NOT_FOUND,"file < %s > not found",configFile.c_str());
        }
      }
      po::parsed_options parsed = parse_config_file(file,config_file_options,true);
      store(parsed,vM);
      po::notify(vM);
      vector<string> additional_parameters;
      additional_parameters = collect_unrecognized(parsed.options,po::include_positional);
      for(vector<string>::iterator vit = additional_parameters.begin();
      vit!=additional_parameters.end();vit++) {
        ierr = PetscPrintf(PETSC_COMM_WORLD,"** WARRNING Unrecognised option %s\n",vit->c_str()); CHKERRQ(ierr);
      }
    } catch (exception& ex) {
      SETERRQ(PETSC_COMM_SELF,MOFEM_STD_EXCEPTION_THROW,"error parsing material elastic configuration file");
    }
    PetscFunctionReturn(0);

  }

  PetscErrorCode setBlocksOrder() {
    PetscFunctionBegin;
    ErrorCode rval;
    PetscErrorCode ierr;
    //set app. order
    PetscBool flg = PETSC_TRUE;
    PetscInt disp_order;
    ierr = PetscOptionsGetInt(PETSC_NULL,"-order",&disp_order,&flg); CHKERRQ(ierr);
    if(flg!=PETSC_TRUE) {
      disp_order = 1;
    }
    for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField,BLOCKSET,it)) {
      if(blockData[it->get_msId()].oRder == -1) continue;
      if(blockData[it->get_msId()].oRder == disp_order) continue;
      PetscPrintf(mField.get_comm(),"Set block %d oRder to %d\n",it->get_msId(),blockData[it->get_msId()].oRder);
      Range block_ents;
      rval = mField.get_moab().get_entities_by_handle(it->meshset,block_ents,true); CHKERR(rval);
      Range ents_to_set_order;
      ierr = mField.get_moab().get_adjacencies(block_ents,3,false,ents_to_set_order,Interface::UNION); CHKERRQ(ierr);
      ents_to_set_order = ents_to_set_order.subset_by_type(MBTET);
      ierr = mField.get_moab().get_adjacencies(block_ents,2,false,ents_to_set_order,Interface::UNION); CHKERRQ(ierr);
      ierr = mField.get_moab().get_adjacencies(block_ents,1,false,ents_to_set_order,Interface::UNION); CHKERRQ(ierr);
      if(mField.check_field("DISPLACEMENT")) {
        ierr = mField.set_field_order(ents_to_set_order,"DISPLACEMENT",blockData[it->get_msId()].oRder); CHKERRQ(ierr);
      }
      if(mField.check_field("SPATIAL_POSITION")) {
        ierr = mField.set_field_order(ents_to_set_order,"SPATIAL_POSITION",blockData[it->get_msId()].oRder); CHKERRQ(ierr);
      }
      if(mField.check_field("DOT_SPATIAL_POSITION")) {
        ierr = mField.set_field_order(ents_to_set_order,"DOT_SPATIAL_POSITION",blockData[it->get_msId()].oRder); CHKERRQ(ierr);
      }
      if(mField.check_field("SPATIAL_VELOCITY")) {
        ierr = mField.set_field_order(ents_to_set_order,"SPATIAL_VELOCITY",blockData[it->get_msId()].oRder); CHKERRQ(ierr);
      }
    }
    PetscFunctionReturn(0);
  }

  #ifdef __NONLINEAR_ELASTIC_HPP

  virtual PetscErrorCode setBlocks(map<int,NonlinearElasticElement::BlockData> &set_of_blocks) {
    PetscFunctionBegin;
    ErrorCode rval;
    PetscErrorCode ierr;
    if(!iNitialized) {
      ierr = iNit(); CHKERRQ(ierr);
      ierr = readConfigFile(); CHKERRQ(ierr);
      ierr = setBlocksOrder(); CHKERRQ(ierr);
      iNitialized = true;
    }
    for(_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(mField,BLOCKSET|MAT_ELASTICSET,it)) {
      int id = it->get_msId();
      Mat_Elastic mydata;
      ierr = it->get_attribute_data_structure(mydata); CHKERRQ(ierr);
      EntityHandle meshset = it->get_meshset();
      rval = mField.get_moab().get_entities_by_type(meshset,MBTET,set_of_blocks[id].tEts,true); CHKERR_PETSC(rval);
      set_of_blocks[id].iD = id;
      if(blockData[id].yOung >= 0) set_of_blocks[id].E = blockData[id].yOung;
      if(blockData[id].pOisson >= -1) set_of_blocks[id].PoissonRatio = blockData[id].pOisson;
      PetscPrintf(mField.get_comm(),"Block Id %d Young Modulus %3.2g Poisson Ration %3.2f Material model %s\n",
      id,set_of_blocks[id].E,set_of_blocks[id].PoissonRatio,blockData[id].mAterial.c_str());
      if(blockData[id].mAterial.compare(MAT_EBERLEINFIBRES_NEOHOOKEAN)==0) {
        set_of_blocks[id].materialDoublePtr = &doubleMaterialModel.at(MAT_EBERLEINFIBRES_NEOHOOKEAN);
        set_of_blocks[id].materialAdoublePtr = &aDoubleMaterialModel.at(MAT_EBERLEINFIBRES_NEOHOOKEAN);
        if(blockData[id].f_k1>= 0) set_of_blocks[id].k1 = blockData[id].f_k1;
        if(blockData[id].f_k2>= 0) set_of_blocks[id].k2 = blockData[id].f_k2;
        if(blockData[id].f_d1_X>= -1) set_of_blocks[id].d1_X = blockData[id].f_d1_X;
        if(blockData[id].f_d1_Y>= -1) set_of_blocks[id].d1_Y = blockData[id].f_d1_Y;
        if(blockData[id].f_d1_Z>= -1) set_of_blocks[id].d1_Z = blockData[id].f_d1_Z;
        if(blockData[id].f_d2_X>= -1) set_of_blocks[id].d2_X = blockData[id].f_d2_X;
        if(blockData[id].f_d2_Y>= -1) set_of_blocks[id].d2_Y = blockData[id].f_d2_Y;
        if(blockData[id].f_d2_Z>= -1) set_of_blocks[id].d2_Z = blockData[id].f_d2_Z;

      } else {
        SETERRQ(PETSC_COMM_SELF,MOFEM_NOT_IMPLEMENTED,"field with that space is not implemented");
      }
    }
    PetscFunctionReturn(0);
  }

  #endif //__NONLINEAR_ELASTIC_HPP

};

#endif // __SOFTTISSUEMATERIALS_HPP__
